import { Item } from '../../interfaces/item.model';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { ModalComponent } from '../../../shared/components/modal/modal.component';
import { CollectionService } from '../../../core/services/collection/collection.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddItemComponent implements OnInit {
  collection: Observable<Item[]>;
  constructor(private _Router: Router, private modalService: NgbModal, private _CollectionService: CollectionService) { }

  ngOnInit() {
    this.collection = this._CollectionService.collection;
  }

  addItem(item: Item) {
    this._CollectionService.addItem(item);
    // this._Router.navigate(['/list']);
    this.openModal('Element ajouté', '/items/list');
  }

  openModal(msg, route) {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.msg = msg;
    modalRef.componentInstance.route = route;
  }

}
