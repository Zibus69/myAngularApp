import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

import { ItemId } from '../../interfaces/item-id.model';
import { State } from '../../../shared/enums/state.enum';
import { CollectionService } from '../../../core/services/collection/collection.service';


@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemComponent implements OnInit {
  @Input('item') item: ItemId;
  state = State;
  constructor(private _CollectionService: CollectionService) { }

  ngOnInit() {
  }

  changeState(item: ItemId, state: State) {
    this._CollectionService.update(item, state);
  }

  deleteItem(item: ItemId): void {
    this._CollectionService.delete(item);
  }

}
