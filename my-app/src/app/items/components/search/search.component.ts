import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import {CollectionService} from '../../../core/services/collection/collection.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  form: FormGroup;
  valueCtrl: FormControl;
  sucription: Subscription;

  constructor(private _FormBuilder: FormBuilder, private _CollectionService: CollectionService) {
    this.valueCtrl = _FormBuilder.control('');
    this.form = _FormBuilder.group({
      value: this.valueCtrl
    });
  }



  ngOnInit(): void {
    this.sucription = this.valueCtrl.valueChanges.debounceTime(300).distinctUntilChanged()
    .map(term => this._CollectionService.search(term)).subscribe();
  }

  ngOnDestroy(): void {
    this.sucription.unsubscribe();
  }

}
