import {ListItemsComponent} from './containers/list-items/list-items.component';
import {PageNotFoundComponent} from '../core/components/page-not-found/page-not-found.component';
import {HomeComponent} from '../core/components/home/home.component';
import {RouterModule, Routes} from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddItemComponent } from './components/add-item/add-item.component';

const itemRoutes: Routes = [
  { path: 'list',      component: ListItemsComponent },
  { path: 'addItem',      component: AddItemComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      itemRoutes
    )
  ],
  declarations: []
})
export class ItemsRoutingModule { }
