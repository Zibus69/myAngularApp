import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ItemComponent } from './components/item/item.component';
import { ListItemsComponent } from './containers/list-items/list-items.component';
import {SharedModule} from '../shared/shared.module';
import { ItemsRoutingModule } from './items-routing.module';
import { AddItemComponent } from './components/add-item/add-item.component';
import { SearchComponent } from './components/search/search.component';

@NgModule({
  imports: [
    CommonModule, SharedModule, ItemsRoutingModule, FormsModule, ReactiveFormsModule
  ],
  declarations: [ItemComponent, ListItemsComponent, AddItemComponent, SearchComponent],
  exports: [ItemComponent, ListItemsComponent]
})
export class ItemsModule { }
