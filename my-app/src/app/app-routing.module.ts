import {Router, RouterModule,  Routes, PreloadAllModules} from '@angular/router';
import {ListItemsComponent} from './items/containers/list-items/list-items.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './core/guards/auth.guard';

const appRoutes: Routes = [
  {
    path: 'items',
    loadChildren: './items/items.module#ItemsModule',
    canActivate: [AuthGuard]
  },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      {preloadingStrategy: PreloadAllModules}
      // { enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: []
})
export class AppRoutingModule {}
