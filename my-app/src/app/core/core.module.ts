import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { CoreRoutingModule } from './core-routing.module';
import { LoginComponent } from './components/login/login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CollectionService } from './services/collection/collection.service';
import { AuthService } from './services/auth/auth.service';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [PageNotFoundComponent, HomeComponent, LoginComponent],
  providers: [CollectionService, AuthService, AuthGuard],
  exports : [
    HomeComponent,
    PageNotFoundComponent
  ]
})
export class CoreModule { }
