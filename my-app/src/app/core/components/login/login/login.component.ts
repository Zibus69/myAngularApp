import {Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  loginCtrl: FormControl;
  passwordCtrl: FormControl;
  constructor(private _FormBuilder: FormBuilder, private _Router: Router) {
    this.loginCtrl = _FormBuilder.control('', [
    Validators.required,
    Validators.minLength(5)
   ]);
   this.passwordCtrl = _FormBuilder.control('', [
    Validators.required,
    Validators.pattern('(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,15})$')
   ]);

   this.form = _FormBuilder.group({
     login: this.loginCtrl,
     password: this.passwordCtrl
   });
  }

  ngOnInit() {
  }

  processForm(): void {
    this._Router.navigate(['/home']);
  }

  isError(champ: string) {
    console.log(this.form.get(champ).errors);
    return this.form.get(champ).dirty && this.form.get(champ).hasError('pattern');
  }

  isErrorLogin(champ: string) {
    console.log(this.form.get(champ).errors);
    return this.form.get(champ).dirty && this.form.get(champ).hasError('minlength');
  }

}
