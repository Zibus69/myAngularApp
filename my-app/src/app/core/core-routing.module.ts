import {ListItemsComponent} from '../items/containers/list-items/list-items.component';
import {RouterModule, Routes} from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LoginComponent } from './components/login/login/login.component';

const coreRoutes: Routes = [
  { path: 'home',      component: HomeComponent },
  { path: 'login',     component: LoginComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      coreRoutes
    )
  ],
  declarations: []
})
export class CoreRoutingModule { }
