import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/switchMap';
import * as firebase from 'firebase/app';


import {Item} from '../../../items/interfaces/item.model';
import { State } from '../../../shared/enums/state.enum';
import { ItemId } from '../../../items/interfaces/item-id.model';

@Injectable()
export class CollectionService {
  private itemsCollection: AngularFirestoreCollection<Item>;
  collection: Observable<ItemId[]>;
  search$: BehaviorSubject<string|null>;

  constructor(private readonly afs: AngularFirestore) {

    this.search$ = new BehaviorSubject(null);
    this.getCollection();
  }

  // get collection
  getCollection() {
    this.collection = this.search$.switchMap(term => {
      return this.afs.collection<Item>('collection', ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      if (term) {
        query = query.where('name' , '==' , term);
        console.log(term);
      } else {
        query = query.orderBy('name');
      }
      return query;
    }).snapshotChanges().map((res) => {
      return res.map(snap => {
        const data = snap.payload.doc.data() as Item;
        const id = snap.payload.doc.id;
        return { id, ...data };
      });
    });
  });
  }

  //search
  search(term: string): void {
    this.search$.next(term);
  }

  // update item
  update(item: ItemId, state: State): void {
    item.state = state;
    this.afs.doc<Item>('collection/' + item.id).update(item);
  }

  // add item
  addItem(item: Item): void {
    this.afs.collection<Item>('collection').add(item);
  }

  // delete item
  delete(item: ItemId): void {
    this.afs.doc<Item>('collection/' + item.id).delete();
  }

}
