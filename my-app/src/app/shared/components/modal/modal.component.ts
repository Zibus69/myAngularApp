import {Router} from '@angular/router';
import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent implements OnDestroy {
  @Input() msg: string;
  @Input() route: string;

  constructor(public activeModal: NgbActiveModal, private _Router: Router) {}

  ngOnDestroy() {
    this._Router.navigate([this.route]);
  }

}
