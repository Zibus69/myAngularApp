import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import {Item} from '../../../items/interfaces/item.model';
import { State } from '../../enums/state.enum';


@Component({
  selector: 'app-form-item',
  templateUrl: './form-item.component.html',
  styleUrls: ['./form-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormItemComponent implements OnInit {
  @Output() newCmd: EventEmitter<Item> = new EventEmitter();
  state = State;
  form: FormGroup;
  nameCtrl: FormControl;
  refCtrl: FormControl;
  stateCtrl: FormControl;
  constructor(private _FormBuilder: FormBuilder) {
    this.nameCtrl = _FormBuilder.control('', [
    Validators.required,
    Validators.minLength(3)
   ]);
   this.refCtrl = _FormBuilder.control('', [
    Validators.required,
    Validators.minLength(4)
   ]);
   this.stateCtrl = _FormBuilder.control(this.state.ALIVRER);

   this.form = _FormBuilder.group({
     name: this.nameCtrl,
     ref: this.refCtrl,
     state: this.stateCtrl
   });
  }

  ngOnInit() {
  }

  processItem(): void {
    // envoyer au parent le contenu de l'item
    this.newCmd.emit({
      name: this.nameCtrl.value,
      reference: this.refCtrl.value,
      state: this.stateCtrl.value
    });
    this.form.reset();
    this.stateCtrl.setValue(this.state.ALIVRER);
  }

  isError(champ: string) {
    return this.form.get(champ).dirty && this.form.get(champ).hasError('minlength');
  }


}
