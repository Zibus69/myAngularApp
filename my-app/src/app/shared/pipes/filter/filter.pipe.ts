import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const searchTerm = args.toLowerCase();
    return searchTerm ? value.filter( (item) => item.name.toLowerCase().indexOf(searchTerm) !== -1) : value;
  }

}
