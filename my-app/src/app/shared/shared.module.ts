import {RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { MenuComponent } from './components/menu/menu.component';
import { FormItemComponent } from './components/form-item/form-item.component';

import { StateDirective } from './directives/state/state.directive';
import { ModalComponent } from './components/modal/modal.component';
import { FilterPipe } from './pipes/filter/filter.pipe';
import { SearchComponent } from '../items/components/search/search.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  declarations: [MenuComponent, StateDirective, FormItemComponent, ModalComponent, FilterPipe, UserProfileComponent],
  exports : [
    MenuComponent,
    StateDirective,
    FormItemComponent,
    ModalComponent,
    FilterPipe
  ],
  entryComponents : [
    ModalComponent
  ]
})
export class SharedModule { }
